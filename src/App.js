import React, { Component } from 'react';
import './App.css';
import Content from './components/index';
import Header from './components/header.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Content />
      </div>
    );
  }
}

export default App;
