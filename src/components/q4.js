import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router-dom'
import { FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import career from './../res/career.png'


class Quest4 extends Component {

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title="Parolele4" />
            <CardText style={{ fontSize: '35px' }}>
              Ce o parola ar trebui sa contina ?
            </CardText>
            <CardActions >
              <Link to="/5"><FlatButton onClick={this.props.triggerRight}  labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Semnele din [A-Z][a-z][0-9]!@#$%*-+." /></Link>
              <Link to="/5"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Numarul tau de telefon sau numele animalului domestic." /></Link>
              <Link to="/5"><FlatButton onClick={this.props.triggerWrong} style={{ marginTop: 10 }} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Nu importa, principalul sa fiiba usor memorabila si tapabila." /></Link>
            </CardActions>
          </Card>
          {/*           <div style={{ marginTop: 30 }}>
            <img src={career}  style={{ height:630}}/>
          </div> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default Quest4;
