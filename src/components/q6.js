import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router-dom'
import { FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import career from './../res/career.png'


class Quest6 extends Component {

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title="Virusurile si OS" />
            <CardText style={{ fontSize: '35px' }}>
              Cum virusurile nimeresc in calculatorul tau ?
            </CardText>
            <CardActions >
              <Link to="/7"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Cineva le infiltreaza." /></Link>
              <Link to="/7"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Sunt adusi de alti virusi, care la rindul sau au fost adusi de alti virusi ..." /></Link>
              <Link to="/7"><FlatButton onClick={this.props.triggerRight} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="E vina mea, nu am fost atent." /></Link>
              <Link to="/7"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Virusi nu exista." /></Link>
            </CardActions>
          </Card>
          {/*           <div style={{ marginTop: 30 }}>
            <img src={career}  style={{ height:630}}/>
          </div> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default Quest6;
