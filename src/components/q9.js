import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router-dom'
import { FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import career from './../res/career.png'


class Quest9 extends Component {

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title="Securitatea in Internet" />
            <CardText style={{ fontSize: '35px' }}>
              In caz ca faci cumparaturi online/introduci date bancare sau alte date confidentiale, de ce ar trebui sa fii sigur ?
            </CardText>
            <CardActions >
              <Link to="/10"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Utilizezi o conexiune HTTP nicicind HTTPS (poti verifica asta in browserul din care accesezi)." /></Link>
              <Link to="/10"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Utilizezi o singura  cartela bancara pentru tot, e sigur DA?" /></Link>
              <Link to="/10"><FlatButton onClick={this.props.triggerRight} style={{ marginTop: 10 }} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Utilizezi o cartela bancara speciala, pentru astfel de operatii si folosesti doar HTTPS." /></Link>
            </CardActions>
          </Card>
          {/*           <div style={{ marginTop: 30 }}>
            <img src={career}  style={{ height:630}}/>
          </div> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default Quest9;
