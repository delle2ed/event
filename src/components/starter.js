import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Link } from 'react-router-dom'
import start from './../res/11st.png'
import { FlatButton } from 'material-ui'

class Start extends Component {
  render() {
    return (
      <MuiThemeProvider>
        {/*  <div style={{ marginTop: 30 }}>
        <img src={start} />
      </div> */}
        <Card>
          <CardHeader />
          <CardText style={{ fontSize: '43px' }}>
             Quiz in Securitatea Informationala V2.0
</CardText>
          <CardActions style={{ marginBottom: 20 }}>
            <Link to="/1"><FlatButton onClick={this.props.newGame} label="Apasa aici daca vrei sa incerci" labelStyle={{ fontSize: '27px' }} /></Link>
          </CardActions>
        </Card>
        <div style={{ marginTop: 50 }}>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default Start;
