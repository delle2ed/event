import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router-dom'
import { FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import career from './../res/career.png'


class Quest11 extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title="Viiirus" />
            <CardText style={{ fontSize: '35px' }}>
              Cum sa te izolezi de contact cu virusi?
            </CardText>
            <CardActions >
              <Link to="/end"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Sa utilizezi doar stick-uri proprii." /></Link>
              <Link to="/end"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Sa nu te conectezi la internet de loc." /></Link>
              <Link to="/end"><FlatButton onClick={this.props.triggerRight} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Nicicum, Sistemul tau operational in permanenta va fi sub risc." /></Link>
            </CardActions>
          </Card>
          {/*           <div style={{ marginTop: 30 }}>
            <img src={career}  style={{ height:630}}/>
          </div> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default Quest11;
