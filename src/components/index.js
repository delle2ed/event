import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Start from './starter.js';
import Last from './final.js';
import Quest1 from './q1.js';
import Quest2 from './q2.js';
import Quest3 from './q3.js';
import Quest4 from './q4.js';
import Quest5 from './q5.js';
import Quest6 from './q6.js';
import Quest7 from './q7.js';
import Quest8 from './q8.js';
import Quest9 from './q9.js';
import Quest10 from './q10.js';
import Quest11 from './q11.js';



class Content extends Component {

  constructor(props) {
    super(props);
    this.state = {
      score: 0,
      from: 0
    }
    this.wrongResponse = this.wrongResponse.bind(this);
    this.rightResponse = this.rightResponse.bind(this);
    this.reset = this.reset.bind(this);
  }

  wrongResponse() {
    this.setState({ from: this.state.from + 1 })
  }

  rightResponse() {
    this.setState({ score: this.state.score + 1 })
    this.setState({ from: this.state.from + 1 })
  }

  reset() {
    this.setState({ score: 0 })
    this.setState({ from: 0 })
  }

  /* onClick={this.props.triggerRight} onClick={this.props.triggerWrong} */
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" render={() => <Start newGame={this.reset} />} />
          <Route path='/1' render={() => <Quest1 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/2' render={() => <Quest2 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/3' render={() => <Quest3 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/4' render={() => <Quest4 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/5' render={() => <Quest5 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/6' render={() => <Quest6 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/7' render={() => <Quest7 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/8' render={() => <Quest8 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/9' render={() => <Quest9 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/10' render={() => <Quest10 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/11' render={() => <Quest11 triggerRight={this.rightResponse} triggerWrong={this.wrongResponse} />} />
          <Route path='/end' render={() => <Last finalScore={this.state.score} finalMax={this.state.from} />} />
          {this.state.score} {this.state.from}
        </div>
      </Router>
    );
  }
}

export default Content;
