import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router-dom'
import { FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import career from './../res/career.png'


class Quest7 extends Component {

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title="Securitatea in Internet" />
            <CardText style={{ fontSize: '35px' }}>
              Ce date personale putem pune in acces liber ?
            </CardText>
            <CardActions >
              <Link to="/8"><FlatButton onClick={this.props.triggerRight} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Doar cea ce e legat de lucru/distractie" /></Link>
              <Link to="/8"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Adresa de resedinta reala, numarul de telefon personal, convorbiri personale" /></Link>
              <Link to="/8"><FlatButton onClick={this.props.triggerWrong} style={{ marginTop: 10 }} labelStyle={{color:'red', fontSize: '30px', textTransform: "none" }} label="TOATE FOTOGRAFIILE, MAI MULTE FOTOGRAFII ZEULUI FOTOGRAFIILOR" /></Link>
            </CardActions>
          </Card>
          {/*           <div style={{ marginTop: 30 }}>
            <img src={career}  style={{ height:630}}/>
          </div> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default Quest7;
