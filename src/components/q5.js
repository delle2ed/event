import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router-dom'
import { FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import career from './../res/career.png'


class Quest5 extends Component {

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title="Securitatea si Retelistica" />
            <CardText style={{ fontSize: '35px' }}>
              Ce ar trebui sa faci cu un Hotspot de Wi-Fi de acasa?
            </CardText>
            <CardActions >
              <Link to="/6"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Sa-l tin fara parola. Libertate la toti ce doresc sa-l folosesca! " /></Link>
              <Link to="/6"><FlatButton onClick={this.props.triggerRight} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Sa-i schimb parola de acces la router si spot, sa ii tin SSID-ul(numele retelei) ascuns." /></Link>
              <Link to="/6"><FlatButton onClick={this.props.triggerWrong} style={{ marginTop: 10 }} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Ascund routerul sub masa, Pff nimeni nu se va putea conecta." /></Link>
            </CardActions>
          </Card>
          {/*           <div style={{ marginTop: 30 }}>
            <img src={career}  style={{ height:630}}/>
          </div> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default Quest5;
