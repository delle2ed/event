import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router-dom'
import { FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import career from './../res/career.png'


class Quest3 extends Component {

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title="Parolele3" />
            <CardText style={{ fontSize: '35px' }}>
              Cit de des e nevoie sa iti modifici parolele ?
            </CardText>
            <CardActions >
              <Link to="/4"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Lunar" /></Link>
              <Link to="/4"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Zilnic" /></Link>
              <Link to="/4"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Nicicind" /></Link>
              <Link to="/4"><FlatButton onClick={this.props.triggerRight} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Odata la 3~6 luni" /></Link>
              <Link to="/4"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Fiecare 5ms" /></Link>
            </CardActions>
          </Card>
          {/*           <div style={{ marginTop: 30 }}>
            <img src={career}  style={{ height:630}}/>
          </div> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default Quest3;
