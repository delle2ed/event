import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router-dom'
import { FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import career from './../res/career.png'


class Quest10 extends Component {

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title="Criptografia" />
            <CardText style={{ fontSize: '35px' }}>
              Cum se dezvolta capacitatile criptografiei in zilele de astazi odata cu cresterea capacitatilor tehnice ?
            </CardText>
            <CardActions >
              <Link to="/11"><FlatButton onClick={this.props.triggerWrong} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Totul e static (Caesar e cel mai bun algoritm-AVE CAESAR)" /></Link>
              <Link to="/11"><FlatButton onClick={this.props.triggerRight} style={{ marginTop: 10 }} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Nu exista algoritm ce nu poate fi 'farmat' e doar intrebare de timp->mult timp, e permanenta necesitate in metode noi." /></Link>
              <Link to="/11"><FlatButton onClick={this.props.triggerWrong} style={{ marginTop: 10 }} labelStyle={{ fontSize: '27px', textTransform: "none" }} label="Criptografia e ceva de ce nu mai avem nevoie, calculatoarele cuantice vor 'destruge' tot HA!" /></Link>
            </CardActions>
          </Card>
          {/*           <div style={{ marginTop: 30 }}>
            <img src={career}  style={{ height:630}}/>
          </div> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default Quest10;
