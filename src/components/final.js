import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { FlatButton } from 'material-ui'
import { Link } from 'react-router-dom'


class Last extends Component {
  constructor(props) {
    super(props);
    this.state = {
      points: Math.round(100*this.props.finalScore/this.props.finalMax),
    }

  }

  render() {
    return (
      <div style={{ marginTop: 80 }} triggerParentUpdate={this.updateThisCounter}>
        <MuiThemeProvider>
          <Card>
            <CardHeader />
            <CardTitle title={"Scorul tau este "+this.state.points+"% din cel maxim."}/>

             <div>
             {this.state.points < 31 && this.state.points > 1 &&
         <h2>
          Eww, e cam rau insa la specialitatea Securitate Informationala vei putea afla raspuns la toate intrebarile si mai mult.
          </h2>
      }

      {this.state.points < 51 && this.state.points > 31 &&
         <h2>
           E satisfacator ,la specialitatea Securitate Informationala vei putea dezvolta capacitatile curente.
          </h2>
      }
       {this.state.points < 71 && this.state.points > 51 &&
         <h2>
            Bine, te asteptam la specialitatea Securitate Informationala, vei putea atinge rezultate bune la noi ^_^.
          </h2>
      }
       {this.state.points < 81 && this.state.points > 71 &&
         <h2>
            Foarte bine, deja te-ai inscris la specialitatea Securitate Informationala ?
          </h2>
      }
      {this.state.points < 91 && this.state.points > 81  &&
         <h2>
            Wow, ai raspuns corect aproape la tot, doresti sa studiezi mai departe Securitate Informationala ?
          </h2>
      }

      {this.state.points < 101 && this.state.points > 91  &&
         <h2>
            *clap**clap* Perfect, sunt sigur ca iti place Securitate Informationala, felicitari.
          </h2>
      }
      
    </div>

            <CardText >
            </CardText>
            <CardActions>
              <Link to="/"><FlatButton label="Spre inceput" /></Link>
            </CardActions>
          </Card>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default Last;
